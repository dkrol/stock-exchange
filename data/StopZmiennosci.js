(() => {
  
  const firstStop = 1.5;
  const secondStop = 2;

  main();

  function main() {
    let rows = getRows();
    let dayDifferences = getDayDifferences(rows);
    let average = calculateAverage(dayDifferences);
    
    
    alert(average * firstStop);
    // console.log(average * firstStop);
    // console.log(average * secondStop);
  }

  function getRows() {
    let table = document.getElementsByClassName('qTableFull')[0];
    let rows = table.getElementsByTagName('tr');
    let monthRows = Array.prototype.slice.apply(rows, [1,31]); 
    return monthRows;
  }  

  function getDayDifferences(rows) {
    return rows.map(getDayDifference);
  }

  function getDayDifference(row) {
    let cols = row.getElementsByTagName('td');
    return calculateDayDifference(cols);
  }

  function calculateDayDifference(cols) {
    const maxColIndex = 2;
    const minColIndex = 3;

    let minDay = parseFloat(cols[minColIndex].textContent);
    let maxDay = parseFloat(cols[maxColIndex].textContent);

    return Math.abs((maxDay - minDay).toFixed(2));
  }

  function calculateAverage(dayDifferences) {
    let sum = dayDifferences.reduce((a, b) => a + b, 0);
    return (sum / dayDifferences.length).toFixed(2);
  }

})();