export interface Action {
  name: string;
  url: string;
}
