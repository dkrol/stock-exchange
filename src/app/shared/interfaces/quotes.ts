export interface Quotes {
  date: string;
  open: number;
  min: number;
  max: number;
  close: number;
  volumen: number;
}
