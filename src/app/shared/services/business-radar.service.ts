import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Action } from '../interfaces/action';
import { HtmlParserService } from './html-parser.service';
import { Quotes } from '../interfaces/quotes';
import { HttpClientService } from './http-client.service';

@Injectable({
  providedIn: 'root'
})
export class BusinessRadarService {
  constructor(private http: HttpClient, private httpClientService: HttpClientService, private htmlParserService: HtmlParserService) { }

  public getGpwActions(): Observable<Array<Action>> {
    const url = '/gielda/akcje_gpw';
    return this.httpClientService.get(url)
      .pipe(map((response: Document) => this.htmlParserService.parseActions(response)));
  }

  public getTodayStockQuotes(url: string): Observable<Quotes> {
    return this.httpClientService.get(url)
      .pipe(map((response: Document) => this.htmlParserService.parseTodayStockQuotes(response)));
  }

  public getHistoryStockQuotes(url: string): Observable<Array<Quotes>> {
    const historyUrl = url.replace('/notowania/', '/notowania-historyczne/');
    return this.httpClientService.get(historyUrl)
      .pipe(map((response: Document) => this.htmlParserService.parseHistoryStockQuotes(response)));
  }
}
