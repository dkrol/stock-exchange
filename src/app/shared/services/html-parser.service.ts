import { Injectable } from '@angular/core';
import { Action } from '../interfaces/action';
import { Quotes } from '../interfaces/quotes';

@Injectable({
  providedIn: 'root'
})
export class HtmlParserService {

  constructor() { }

  public  parseActions(doc: Document) {
    const rows = this.getTableRowsByClass(doc, 'qTableFull');
    const column = this.getTableColumn(rows, 0);
    const actions = this.getFirstChildElement(column, 'a');

    return this.parseLinkToAction(actions);
  }

  public getTableRowsByClass(doc: Document, className: string) {
    const table = doc.getElementsByClassName(className)[0];
    return Array.from(table.getElementsByTagName('tr'));
  }

  public getTableRowsById(doc: Document, id: string) {
    const table = doc.getElementById(id);
    return Array.from(table.getElementsByTagName('tr'));
  }

  public getTableColumn(rows: Array<any>, columnIndex: number): Array<any> {
    return rows.map(tr => tr.getElementsByTagName('td')[columnIndex]).filter(tr => tr);
  }

  public getFirstChildElement(arr: Array<any>, tagName: string): Array<Document> {
    return arr.map(td => td.getElementsByTagName(tagName)[0]).filter(a => a);
  }

  public parseLinkToAction(actions: Array<any>): Array<Action> {
    const result: Array<Action> = [];

    for (const action of actions) {
      result.push({
        name: action.innerText,
        url: action.href
      });
    }

    return result;
  }

  public parseTodayStockQuotes(doc: Document) {
    const rows = this.getTableRowsById(doc, 'profileSummaryCurrent');
    const columns = this.getTableColumn(rows, 0);

    const result: Quotes = {
      date: columns[0].innerText,
      close: parseFloat(columns[1].innerText),
      open: parseFloat(columns[2].innerText),
      min: parseFloat(columns[3].innerText),
      max: parseFloat(columns[4].innerText),
      volumen: parseFloat(columns[5].innerText.replace(/\s+/g, ''))
    };

    return result;
  }

  public parseHistoryStockQuotes(doc: Document): Array<Quotes> {
    const result: Array<Quotes> = [];
    const rows = this.getTableRowsByClass(doc, 'qTableFull');

    const today = new Date();
    const startIndex = today.getDay() === 6 || today.getDay() === 0 ? 2 : 1;

    for (let i = startIndex; i < rows.length; i++) {
      result.push(this.prepareQuotesByHistoryRow(rows[i]));
    }

    return result;
  }

  private prepareQuotesByHistoryRow(row): Quotes {
    const data = row.getElementsByTagName('td');
    const result: Quotes = {
      date: data[0].innerText,
      open: parseFloat(data[1].innerText),
      max: parseFloat(data[2].innerText),
      min: parseFloat(data[3].innerText),
      close: parseFloat(data[4].innerText),
      volumen: parseFloat(data[5].innerText.replace(/\s+/g, ''))
    };

    return result;
  }
}
