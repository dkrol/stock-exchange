import { Injectable } from '@angular/core';
import { Quotes } from '../interfaces/quotes';

@Injectable({
  providedIn: 'root'
})
export class StockCalculatorService {

  constructor() { }

  public calculateDayDifferenceAverage(todayQuotes: Quotes, historyQuotes: Array<Quotes>, days: number): number {
    const quotes = historyQuotes.slice();
    quotes.unshift(todayQuotes);

    const dayDifferences = this.getDayDifferences(quotes, days);
    const dayDifferencesAverage = this.calculateAverage(dayDifferences);

    return dayDifferencesAverage;
  }

  public calculateStopVariationSmall(todayQuotes: Quotes, historyQuotes: Array<Quotes>, days: number) {
    const dayDifferenceAverage = this.calculateDayDifferenceAverage(todayQuotes, historyQuotes, days);
    return parseFloat((todayQuotes.min - (dayDifferenceAverage * 1.5)).toFixed(2));
  }

  public calculateStopVariationLarge(todayQuotes: Quotes, historyQuotes: Array<Quotes>, days: number) {
    const dayDifferenceAverage = this.calculateDayDifferenceAverage(todayQuotes, historyQuotes, days);
    return parseFloat((todayQuotes.min - (dayDifferenceAverage * 2)).toFixed(2));
  }

  private getDayDifferences(quotes: Array<Quotes>, days: number): Array<number> {
    const result: Array<number> = [];

    for (let i = 0; i < days; i++) {
      result.push(this.calculateDayDifference(quotes[i]));
    }

    return result;
  }

  private calculateDayDifference(quotes: Quotes): number {
    return  parseFloat((Math.abs(quotes.max - quotes.min)).toFixed(2));
  }

  private calculateAverage(dayDifferences: Array<number>) {
    const sum = dayDifferences.reduce((a, b) => a + b, 0);
    return parseFloat((sum / dayDifferences.length).toFixed(2));
  }
}
