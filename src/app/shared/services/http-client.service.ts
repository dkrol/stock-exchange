import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/internal/operators/map';

@Injectable({
  providedIn: 'root'
})
export class HttpClientService {

  private readonly headers: HttpHeaders = new HttpHeaders().set('Content-Type', 'text/html; charset=utf-8');

  constructor(private http: HttpClient) { }

  public get(url: string): Observable<Document> {
    return this.http.get(url, { headers: this.headers, responseType: 'text' })
      .pipe(map((response: string) => (new DOMParser()).parseFromString(response, 'text/html')));
  }
}
