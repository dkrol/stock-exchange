import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { BusinessRadarService } from 'src/app/shared/services/business-radar.service';
import { Action } from 'src/app/shared/interfaces/action';

@Component({
  selector: 'se-stock',
  templateUrl: './stock.component.html',
  styleUrls: ['./stock.component.less']
})
export class StockComponent implements OnInit {

  private actions: Array<Action> = [];
  private filteredActions: Array<Action> = [];
  private selectedAction: Action = { name: '', url: '' };

  @Output() selectedActionChange = new EventEmitter();

  constructor(private businessRadarService: BusinessRadarService) { }

  ngOnInit() {
    this.initStock();
    this.selectedActionChange.emit(this.selectedAction);
  }

  initStock() {
    this.businessRadarService.getGpwActions()
      .subscribe(response => {
        this.actions = response;
        this.filteredActions = response;
      });
  }

  actionClick(action: Action) {
    this.selectedAction = action;
    this.selectedActionChange.emit(this.selectedAction);
  }

  searchValueChange(event) {
    this.filteredActions = this.actions.filter(action => action.name.includes(event.toUpperCase()));
  }
}
