import { Component, OnInit } from '@angular/core';
import { BusinessRadarService } from 'src/app/shared/services/business-radar.service';

@Component({
  selector: 'se-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.less']
})
export class DashboardComponent implements OnInit {

  constructor(private businessRadarService: BusinessRadarService) { }

  ngOnInit() { }
}
