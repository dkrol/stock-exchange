import { Component, OnInit, Input } from '@angular/core';
import { Action } from 'src/app/shared/interfaces/action';
import { BusinessRadarService } from 'src/app/shared/services/business-radar.service';
import { Quotes } from 'src/app/shared/interfaces/quotes';
import { StockCalculatorService } from 'src/app/shared/services/stock-calculator.service';

@Component({
  selector: 'se-action',
  templateUrl: './action.component.html',
  styleUrls: ['./action.component.less']
})
export class ActionComponent implements OnInit {

  constructor(private businessRadarService: BusinessRadarService, private stockCalculatorService: StockCalculatorService) { }

  private action: Action;
  private quotes: Quotes;
  private historyQuotes: Array<Quotes>;

  private stopVariationSmall: number;
  private stopVariationLarge: number;

  @Input()
  set selectedAction(action: Action) {
    this.action = action;
    this.updateQuotes();
  }

  ngOnInit() {
  }

  updateQuotes() {
    if (this.action && this.action.url !== '') {
      this.businessRadarService.getTodayStockQuotes(this.action.url)
        .subscribe(response => {
          this.quotes = response;
          this.updateHistoryQuotes();
        });
    }
  }

  updateHistoryQuotes() {
    if (this.action && this.action.url !== '') {
      this.businessRadarService.getHistoryStockQuotes(this.action.url)
        .subscribe(response => {
          this.historyQuotes = response;
          this.calculateStopVariations();
        });
    }
  }

  calculateStopVariations() {
    if (this.quotes && this.historyQuotes) {
      this.stopVariationSmall = this.stockCalculatorService.calculateStopVariationSmall(this.quotes, this.historyQuotes, 30);
      this.stopVariationLarge = this.stockCalculatorService.calculateStopVariationLarge(this.quotes, this.historyQuotes, 30);
    }
  }
}
